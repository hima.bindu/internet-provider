package fetcher

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"net/http"
	"shabby-internet-provider/search"
)

type HttpClient struct {
	Client http.Client
	URL    string
}

func (client *HttpClient) FetchBill(id int) (search.BillEntry, error) {
	endpoint := "/internet-bills/v1"
	url := fmt.Sprintf(client.URL+"%s/%d", endpoint, id)
	resp, err := client.Client.Get(url)
	if err != nil {
		return search.BillEntry{}, err
	}

	if resp.StatusCode != 200 {
		return search.BillEntry{}, errors.New("Unable to fetch bill " + resp.Status)
	}

	defer resp.Body.Close()

	billEntry, err := parseCSVResponse(resp.Body)

	return billEntry, nil
}

func parseCSVResponse(body io.Reader) (search.BillEntry, error) {
	reader := csv.NewReader(body)
	reader.FieldsPerRecord = -1
	data, err := reader.ReadAll()
	if err != nil {
		return search.BillEntry{}, err
	}

	var billEntry search.BillEntry
	for i, row := range data {
		if i == 0 {
			continue
		}
		// dateString := "01-01-2022"
		// date, err := time.Parse(dateString, row[4])
		// if err != nil {
		// 	date = time.Time{}
		// }
		billEntry = search.BillEntry{
			Id:       row[0],
			Name:     row[1],
			Address:  row[2],
			PlanName: row[3],
			Date:     row[4],
			Amount:   row[5],
		}
		break
	}
	return billEntry, nil
}
