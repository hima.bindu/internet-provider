package fetcher_test

import (
	"net/http"
	"net/http/httptest"
	"shabby-internet-provider/fetcher"
	"shabby-internet-provider/search"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_GetSuccessResponseWhenIdIs1900(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		data := []byte("id,name,address,plan-name,date,amount\n1900,Ram,Kolkata,SuperSaver,01-02-2022,86")
		w.Write(data)
	}))

	client := fetcher.HttpClient{Client: *server.Client(), URL: server.URL}

	defer server.Close()

	billEntry, err := client.FetchBill(1)

	assert.Nil(t, err)
	assert.NotNil(t, billEntry)
	assert.Equal(t, billEntry.Id, "1900")
}

func Test_GetFailureResponseWhenIdIs1(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))

	client := fetcher.HttpClient{Client: *server.Client(), URL: server.URL}

	defer server.Close()

	billEntry, err := client.FetchBill(1)
	assert.NotNil(t, err)
	assert.Equal(t, search.BillEntry{}, billEntry)
}
