package main

import (
	"fmt"
	"net/http"
	"shabby-internet-provider/fetcher"
	"shabby-internet-provider/search"
)

func main() {
	client := fetcher.HttpClient{
		Client: http.Client{
			Transport: &http.Transport{MaxConnsPerHost: 100},
		},
		URL: "http://localhost:8080",
	}

	fmt.Print(search.SearchForBill(&client, "Ram", 1902))
}
