package search_test

import (
	"errors"
	mock_client "shabby-internet-provider/mocks"
	"shabby-internet-provider/search"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func Test_SearchForDesiredBill(t *testing.T) {
	client := mock_client.NewMockFetcher(gomock.NewController(t))
	expected := search.BillEntry{
		Id:   "1",
		Name: "Ram",
	}
	client.EXPECT().FetchBill(1).Return(expected, nil)
	actual, err := search.SearchForBill(client, "Ram", 2)

	assert.Nil(t, err)
	assert.Equal(t, expected, actual)
}

func Test_NoBillFound(t *testing.T) {
	client := mock_client.NewMockFetcher(gomock.NewController(t))
	client.EXPECT().FetchBill(1).Return(search.BillEntry{}, errors.New("Unable to fetch bill"))
	expected := search.BillEntry{}

	actual, err := search.SearchForBill(client, "Ram", 2)

	assert.NotNil(t, err)
	assert.Equal(t, expected, actual)
}