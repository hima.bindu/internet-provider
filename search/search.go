package search

import (
	"errors"
	"fmt"
	"sync"
)

type BillEntry struct {
	Id       string
	Name     string
	Address  string
	PlanName string
	Date     string
	Amount   string
}

type Fetcher interface {
	FetchBill(id int) (BillEntry, error)
}

// func SearchForBill(fetcher Fetcher, name string, endId int) (BillEntry, error) {
// 	for i := 1; i < endId; i++ {
// 		bill, _ := fetcher.FetchBill(i)
// 		if bill.Id == "1" {
// 			return bill, nil
// 		}
// 	}
// 	return BillEntry{}, errors.New("No bills found")
// }

func SearchForBill(fetcher Fetcher, name string, endId int) (BillEntry, error) {
	idsChan := make(chan int, 100)
	go func() {
		for i := 1; i < endId; i++ {
			idsChan <- i
		}
		close(idsChan)
	}()

	billChan := make(chan BillEntry)
	go func() {
		var wg sync.WaitGroup
		for id := range idsChan {
			wg.Add(1)
			go sendRequest(id, fetcher, &wg, billChan)
		}
		wg.Wait()
		close(billChan)
	}()
	for bill := range billChan {
		// if bill.Name != "" {
		// 	fmt.Println(bill)
		// }
		if bill.Name == name {
			return bill, nil
		}
	}
	return BillEntry{}, errors.New("No bills found")
	}

func sendRequest(id int, fetcher Fetcher, wg *sync.WaitGroup, billChan chan BillEntry) {
	defer wg.Done()
	bill, err := fetcher.FetchBill(id)
	if err != nil {
		// fmt.Printf("Got an error at id %d with %s\n", id, err)
	}
	billChan <- bill
}
